package nl.benkhard.assessment.request;

import com.google.gson.annotations.SerializedName;

public class OAuthTokenRequest {
    @SerializedName("grant_type")
    private String grantType;
    private String username;
    private String password;

    public OAuthTokenRequest() {
    }

    public OAuthTokenRequest(String grantType, String username, String password) {
        this.grantType = grantType;
        this.username = username;
        this.password = password;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
