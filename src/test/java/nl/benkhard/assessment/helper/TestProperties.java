package nl.benkhard.assessment.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestProperties {
    private static TestProperties instance;
    private final String DEFAULT_PROPERTIES_FILE = "./test-defaults.properties";
    private final String OPTIONAL_PROPERTIES_FILE = "./test.properties";
    private Properties properties;
    private Logger logger = LoggerFactory.getLogger(getClass().getName());

    public static TestProperties getInstance() {
        if(instance == null)
            instance = new TestProperties();
        return instance;
    }

    private TestProperties() {
        Properties defaults = loadProperties(DEFAULT_PROPERTIES_FILE, false);
        properties = loadPropertiesWithDefault(defaults, OPTIONAL_PROPERTIES_FILE, true);
    }

    private Properties loadProperties(String fileName, boolean optional) {
        Properties defaults = new Properties();
        return loadPropertiesWithDefault(defaults, fileName, optional);
    }

    private Properties loadPropertiesWithDefault(Properties defaults, String fileName, boolean optional) {
        Properties properties = new Properties(defaults);
        InputStream input = ClassLoader.getSystemClassLoader().getResourceAsStream(fileName);

        if(input == null) {
            logger.info(String.format("Could not find optional configuration file '%s', skipping.'.", fileName));
            return properties;
        }

        try {
            properties.load(input);
        } catch (IOException e) {
            if(!optional)
                throw new RuntimeException(String.format("Failed to load non-optional configuration file '%s', reason: %s", fileName, e.getMessage()));
        }

        return properties;
    }

    public String get(String key) {
        return properties.getProperty(key);
    }
}
