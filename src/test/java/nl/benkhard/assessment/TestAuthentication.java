package nl.benkhard.assessment;

import org.junit.Test;

import static io.restassured.RestAssured.given;

public class TestAuthentication extends BaseTest {
    @Test
    public void returnsUnauthorizedWithoutCredentials() {
        given()
                .baseUri(baseUrl)
                .auth()
                    .none()
        .when()
                .get("/api/v4/issues")
        .then()
                .statusCode(401);
    }

    @Test
    public void returnsOkWithValidCredentials() {
        given()
                .baseUri(baseUrl)
                .auth()
                .oauth2(getAccessToken())
        .when()
                .get("/api/v4/issues")
        .then()
                .statusCode(200);
    }
}
