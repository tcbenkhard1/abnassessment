package nl.benkhard.assessment;

import com.google.gson.Gson;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import nl.benkhard.assessment.model.Issue;
import nl.benkhard.assessment.request.EditIssueRequest;
import nl.benkhard.assessment.request.NewIssueRequest;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class TestEditIssue extends BaseTest {
    @Test
    public void canChangeTitle() {
        NewIssueRequest request = new NewIssueRequest("This is the old title", "With a random description.");
        // Create a new issue
        Response response =
        given()
                .baseUri(baseUrl)
                .auth()
                .preemptive()
                .oauth2(getAccessToken())
                .contentType(ContentType.JSON)
                .body(request)
                .pathParam("projectId", projectId)
        .when()
                .post(String.format("/api/v4/projects/{projectId}/issues", projectId));
        Assert.assertEquals(201, response.statusCode());

        // Change the title
        Issue issue = new Gson().fromJson(response.getBody().asString(), Issue.class);
        EditIssueRequest editRequest = new EditIssueRequest(issue);
        editRequest.setTitle("This is the new title");

        Response editResponse =
                given()
                        .baseUri(baseUrl)
                        .auth()
                        .preemptive()
                        .oauth2(getAccessToken())
                        .contentType(ContentType.JSON)
                        .body(editRequest)
                        .pathParam("projectId", projectId)
                        .pathParam("issueId", issue.getIid())
                        .when()
                        .put("/api/v4/projects/{projectId}/issues/{issueId}");

        // Verify the title is changed
        response =
        given()
                .baseUri(baseUrl)
                .auth()
                .preemptive()
                .oauth2(getAccessToken())
                .pathParam("issueId", issue.getIid())
                .pathParam("projectId", issue.getProjectId())
        .when()
                .get("/api/v4/projects/{projectId}/issues/{issueId}");

        Issue newIssue = new Gson().fromJson(response.getBody().asString(), Issue.class);
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertEquals(editRequest.getTitle(), newIssue.getTitle());
    }
}
