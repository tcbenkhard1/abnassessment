package nl.benkhard.assessment.model;

public class InvalidPropertyException extends RuntimeException {
    private String property;

    public InvalidPropertyException(String property) {
        this.property = property;
    }

    @Override
    public String getMessage() {
        return String.format("Invalid property %s, please set the property in the test.properties file.");
    }
}
