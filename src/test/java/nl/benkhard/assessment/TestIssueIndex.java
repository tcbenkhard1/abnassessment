package nl.benkhard.assessment;

import com.google.gson.Gson;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import nl.benkhard.assessment.model.Issue;
import nl.benkhard.assessment.request.EditIssueRequest;
import nl.benkhard.assessment.request.NewIssueRequest;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static io.restassured.RestAssured.given;

public class TestIssueIndex extends BaseTest {

    @Test
    public void filterOnOpenShowsLatestOpen() {
        NewIssueRequest request = new NewIssueRequest("This is an open issue", "With a random description.");
        // Create a new issue
        Response response =
        given()
                .baseUri(baseUrl)
                .auth()
                .preemptive()
                .oauth2(getAccessToken())
                .contentType(ContentType.JSON)
                .body(request)
                .pathParam("projectId", projectId)
        .when()
                .post(String.format("/api/v4/projects/{projectId}/issues", projectId));
        Assert.assertEquals(201, response.statusCode());

        // Set state to open
        Issue issue = new Gson().fromJson(response.getBody().asString(), Issue.class);
        EditIssueRequest editRequest = new EditIssueRequest(issue);

        Response editResponse =
        given()
                .baseUri(baseUrl)
                .auth()
                .preemptive()
                .oauth2(getAccessToken())
                .contentType(ContentType.JSON)
                .body(editRequest)
                .pathParam("projectId", projectId)
                .pathParam("issueId", issue.getIid())
        .when()
                .put("/api/v4/projects/{projectId}/issues/{issueId}");

        // Request open issues
        response =
        given()
                .baseUri(baseUrl)
                .auth()
                .preemptive()
                .oauth2(getAccessToken())
        .when()
                .queryParam("state", "opened")
                .get(String.format("/api/v4/issues"));

        Issue[] issues = new Gson().fromJson(response.getBody().asString(), Issue[].class);
        Assert.assertTrue(Arrays.stream(issues).anyMatch(i -> i.getId().equals(issue.getId())));
    }

    @Test
    public void filterOnClosedDoesNotShowLatestOpen() {
        NewIssueRequest request = new NewIssueRequest("This is an open issue", "With a random description.");
        // Create a new issue
        Response response =
                given()
                        .baseUri(baseUrl)
                        .auth()
                        .preemptive()
                        .oauth2(getAccessToken())
                        .contentType(ContentType.JSON)
                        .body(request)
                        .pathParam("projectId", projectId)
                .when()
                        .post("/api/v4/projects/{projectId}/issues");
        Assert.assertEquals(201, response.statusCode());

        // Set state to open
        Issue issue = new Gson().fromJson(response.getBody().asString(), Issue.class);
        EditIssueRequest editRequest = new EditIssueRequest(issue);


        Response editResponse =
                given()
                        .baseUri(baseUrl)
                        .auth()
                        .preemptive()
                        .oauth2(getAccessToken())
                        .contentType(ContentType.JSON)
                        .body(editRequest)
                        .pathParam("projectId", projectId)
                        .pathParam("issueId", issue.getIid())
                .when()
                        .put("/api/v4/projects/{projectId}/issues/{issueId}");

        // Request open issues
        response =
                given()
                        .baseUri(baseUrl)
                        .auth()
                        .preemptive()
                        .oauth2(getAccessToken())
                        .when()
                        .queryParam("state", "closed")
                        .get(String.format("/api/v4/issues"));

        Issue[] issues = new Gson().fromJson(response.getBody().asString(), Issue[].class);
        Assert.assertTrue(Arrays.stream(issues).noneMatch(i -> i.getId().equals(issue.getId())));
    }
}
