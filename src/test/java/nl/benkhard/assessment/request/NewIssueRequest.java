package nl.benkhard.assessment.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewIssueRequest {
    private String title;
    private String description;
    private boolean confidential;
    @SerializedName("asignee_ids")
    private int[] assigneeIds;
    @SerializedName("milestoneId")
    private int milestoneId;
    @Expose(serialize = false)
    private String labels = "";
    @SerializedName("created_at")
    private String createdAt = "";
    @SerializedName("due_date")
    private String dueDate = "";
    private int weight;

    public NewIssueRequest() {
    }

    public NewIssueRequest(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isConfidential() {
        return confidential;
    }

    public void setConfidential(boolean confidential) {
        this.confidential = confidential;
    }

    public int[] getAssigneeIds() {
        return assigneeIds;
    }

    public void setAssigneeIds(int[] assigneeIds) {
        this.assigneeIds = assigneeIds;
    }

    public int getMilestoneId() {
        return milestoneId;
    }

    public void setMilestoneId(int milestoneId) {
        this.milestoneId = milestoneId;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
