package nl.benkhard.assessment;

import io.restassured.http.ContentType;
import nl.benkhard.assessment.request.NewIssueRequest;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class TestIssueCreation extends BaseTest {

    @Test
    public void canCreateNewIssueAndReturnsCreated() {
        given()
                .baseUri(baseUrl)
                .auth()
                .preemptive()
                .oauth2(getAccessToken())
                .contentType(ContentType.JSON)
                .body(new NewIssueRequest("This is a test issue", "With a random description."))
        .when()
                .post(String.format("/api/v4/projects/%d/issues", projectId))
        .then()
                .statusCode(201);
    }

    @Test
    public void returnsBadRequestOnMissingParameter() {
        given()
                .baseUri(baseUrl)
                .auth()
                .preemptive()
                .oauth2(getAccessToken())
                .contentType(ContentType.JSON)
                .body(new NewIssueRequest(null, "With a random description."))
        .when()
                .post(String.format("/api/v4/projects/%d/issues", projectId))
                .then()
                .statusCode(400);
    }

    @Test
    public void returnsNotFoundOnNonExistingProject() {
        int projectId = 1939939399;
        given()
                .baseUri(baseUrl)
                .auth()
                .preemptive()
                .oauth2(getAccessToken())
                .contentType(ContentType.JSON)
                .body(new NewIssueRequest("This is a test issue", "With a random description."))
        .when()
                .post(String.format("/api/v4/projects/%d/issues", projectId))
        .then()
                .statusCode(404);
    }
}
