package nl.benkhard.assessment;

import com.google.gson.Gson;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import nl.benkhard.assessment.helper.TestProperties;
import nl.benkhard.assessment.model.InvalidPropertyException;
import nl.benkhard.assessment.model.OAuthToken;
import nl.benkhard.assessment.request.OAuthTokenRequest;

import static io.restassured.RestAssured.given;

public class BaseTest {
    protected TestProperties properties;
    protected String baseUrl;
    protected String email;
    protected String password;
    protected int projectId;

    protected BaseTest() {
        properties = TestProperties.getInstance();
        setBaseUrl(properties.get("base.url"));
        setEmail(properties.get("user.email"));
        setPassword(properties.get("user.password"));
        setProjectId(Integer.parseInt(properties.get("project.id")));
    }

    private void setProjectId(int projectId) {
        if(projectId == 0)
            throw new InvalidPropertyException("project.id");

        this.projectId = projectId;
    }

    private void setPassword(String password) {
        if(password == null || password.isEmpty())
            throw new InvalidPropertyException("user.password");

        this.password = password;
    }

    private void setEmail(String email) {
        if(email == null || email.isEmpty())
            throw new InvalidPropertyException("user.email");

        this.email = email;
    }

    private void setBaseUrl(String baseUrl) {
        if(baseUrl == null || baseUrl.isEmpty())
            throw new InvalidPropertyException("base.url");

        this.baseUrl = baseUrl;
    }

    private OAuthToken oAuthToken;

    protected String getAccessToken() {
        if(oAuthToken == null)
            getOAuthToken();

        return oAuthToken.getAccessToken();
    }

    private void getOAuthToken() {
        Response response =
                given()
                        .baseUri(baseUrl)
                        .contentType(ContentType.JSON)
                        .body(new OAuthTokenRequest("password", email, password))
                        .when()
                        .post("/oauth/token");

        oAuthToken = new Gson().fromJson(response.getBody().asString(), OAuthToken.class);
    }
}
