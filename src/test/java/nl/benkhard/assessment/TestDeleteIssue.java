package nl.benkhard.assessment;

import com.google.gson.Gson;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import nl.benkhard.assessment.model.Issue;
import nl.benkhard.assessment.request.NewIssueRequest;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class TestDeleteIssue extends BaseTest {

    @Test
    public void returnsStatusDeleted_And_NotFoundAfterDelete() {
        NewIssueRequest request = new NewIssueRequest("Moet verwijderd worden", "With a random description.");

        // Create a new issue
        Response response =
                given()
                        .baseUri(baseUrl)
                        .auth()
                        .preemptive()
                        .oauth2(getAccessToken())
                        .contentType(ContentType.JSON)
                        .body(request)
                        .pathParam("projectId", projectId)
                .when()
                        .post("/api/v4/projects/{projectId}/issues");
        Assert.assertEquals(201, response.statusCode());

        Issue issue = new Gson().fromJson(response.getBody().asString(), Issue.class);
        Assert.assertNotNull(issue);

        // Delete the issue
        given()
                .baseUri(baseUrl)
                .auth()
                .preemptive()
                .oauth2(getAccessToken())
                .pathParam("issueId", issue.getIid())
                .pathParam("projectId", issue.getProjectId())
        .when()
                .delete("/api/v4/projects/{projectId}/issues/{issueId}")
        .then()
                .statusCode(204);

        // Verify issue is removed
        given()
                .baseUri(baseUrl)
                .auth()
                .preemptive()
                .oauth2(getAccessToken())
                .pathParam("issueId", issue.getIid())
                .pathParam("projectId", issue.getProjectId())
        .when()
                .get("/api/v4/projects/{projectId}/issues/{issueId}")
        .then()
                .statusCode(404);
    }

    @Test
    public void returnsNotFoundWhenIssueDoesNotExist() {
        // Delete the issue
        given()
                .baseUri(baseUrl)
                .auth()
                .preemptive()
                .oauth2(getAccessToken())
                .pathParam("issueId", "123")
                .pathParam("projectId", "321")
        .when()
                .delete("/api/v4/projects/{projectId}/issues/{issueId}")
        .then()
                .statusCode(404);
    }
}
