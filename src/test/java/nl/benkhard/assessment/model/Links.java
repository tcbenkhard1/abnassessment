package nl.benkhard.assessment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links {

    @SerializedName("self")
    @Expose
    private String self;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("award_emoji")
    @Expose
    private String awardEmoji;
    @SerializedName("project")
    @Expose
    private String project;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getAwardEmoji() {
        return awardEmoji;
    }

    public void setAwardEmoji(String awardEmoji) {
        this.awardEmoji = awardEmoji;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

}
