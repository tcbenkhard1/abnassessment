# Assessment Timo Benkhard

In this project you will find the files needed to test the Gitlab.com issues api. Below is a description of how to get everything up and running.

**Please note that the gitlab API will block issue creation requests after a large amount of requests**. The quickest way to resolve this is to create a new project and set the project id in the properties file. In a normal test environment I would likely disable throttling.  

## Explanation
1. The project has been setup to be able to connect to different endpoints, allowing for multiple environments to be tested. 
2. Username and password are used to retrieve an OAUTH2 token, that is used to authenticate the other calls.
3. All basic CRUD operations are present.
4. Test can be executed repeatedly, without changing the code, or updating the database.
5. Test are executed using the JUNit framework, creating a JUnit test report that can be read into most CI/CD solutions.

If I had access to the infrastructure I would create a testdatabase and load necessary data using either queries or database versioning tools such as flyway or liquibase, allowing for even faster and more stable test execution. 

## Setup

In order to run the tests, a file called 'test.properties' is needed. This file will be read like a java properties file, similar to this example:

```
user.email=myuser@gmail.com
user.password=secret
project.id=12345
```

In order to run the tests just run the ``mvn test`` command from the project root, or use your favorite IDE.

## Property specification
``user.email`` The email address of the Gitlab.com account that is used for authentication

``user.password`` The password of the Gitlab.com account that is used for authentication 

``project.id`` The ID of the project that can be used for testing

``base.url`` The hostname of the Gitlab instance to be tested against, defaults to ``https://gitlab.com``

 