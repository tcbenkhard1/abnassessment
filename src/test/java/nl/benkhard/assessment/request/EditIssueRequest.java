package nl.benkhard.assessment.request;

import com.google.gson.annotations.SerializedName;
import nl.benkhard.assessment.model.Issue;

public class EditIssueRequest extends NewIssueRequest {
    private int id;
    private int iid;
    @SerializedName("state_event")
    private String stateEvent;

    public EditIssueRequest(Issue issue) {
        super(issue.getTitle(), issue.getDescription());
        this.id = issue.getId();
        this.iid = issue.getIid();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIid() {
        return iid;
    }

    public void setIid(int iid) {
        this.iid = iid;
    }

    public String getStateEvent() {
        return stateEvent;
    }

    public void setStateEvent(String stateEvent) {
        this.stateEvent = stateEvent;
    }
}
